import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ImageService} from '../common/image/image.service';
import {UserService} from '../common/user/user.service';
import {UserImage} from '../common/image/model';

@Component({
  selector: 'user-image-list',
  templateUrl: './user-image-list.component.html',
  styleUrls: ['./user-image-list.component.sass']
})
export class UserImageListComponent implements OnInit {

  @Output()
  users: EventEmitter<string> = new EventEmitter<string>();

  private images: UserImage[] = [];
  private currentImageId = null;

  constructor(private imageService: ImageService, private userService: UserService) { }

  ngOnInit() {
    this.getImagesList();
  }

  getImagesList() {
    this.imageService.getUsersImages().then(images => {
      this.images = images['photos'];
      console.log(images);

      if (this.images.length > 6) { this.images = this.images.slice(0, 6); }
    });
  }

  getUsers(photoId: string) {
    this.users.emit(photoId);
    this.currentImageId = photoId;
  }

  isUserLoading() {
    return this.userService.getIsLoading();
  }
}
