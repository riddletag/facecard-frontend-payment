import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {User} from '../user/model';
import {Card, PaymentMethod} from './model';
import {Observable} from 'rxjs';
import {Response} from '@angular/http';

@Injectable()
export class PaymentService {

  private paymentsApiUrl = `${environment.apiUrl}/payments`;
  private isLoadingUserPayments = false;

  constructor(private http: HttpClient) {
  }

  getIsLoadingUserPayments() {
      return this.isLoadingUserPayments;
  }

  getUserPayments(profileId: string): Promise<Card[]> {
    let params = new HttpParams();
    params = params.append('profileId', profileId);

    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    this.isLoadingUserPayments = true;
    return this.http
      .get<Card[]>(`${this.paymentsApiUrl}/cards`, { params: params, headers: headers })
      .pipe(map(aliases => {
        this.isLoadingUserPayments = false;
        return aliases;
      }))
      .toPromise();
  }

  pay(paymentMethod: PaymentMethod, profileId: string) {
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');

    const paymentRequest = {
      pin: paymentMethod.pin,
      amount: paymentMethod.amount,
      alias: paymentMethod.cardAlias,
      profileId: profileId
    };

    return this.http.post(`${this.paymentsApiUrl}`, paymentRequest, {headers: headers});

  }

  private extractData(res: Response) {
    const body = res.json();
    return body || { };
  }


  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
