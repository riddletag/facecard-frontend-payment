import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserImage} from '../common/image/model';

@Component({
  selector: 'user-image',
  templateUrl: './user-image.component.html',
  styleUrls: ['./user-image.component.sass']
})
export class UserImageComponent implements OnInit {

  @Output()
  getUsers: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  image: UserImage;
  @Input()
  currentId: null;
  @Input()
  isLoading: false;

  constructor() { }

  ngOnInit() {
  }

  getImage() {
    return 'data:image/png;base64,' + this.image.base64Photo;
  }

  isCurrent() {
    return this.image.photoId === this.currentId;
  }

  isCurrentReady() {
    return this.isCurrent() && !this.loading();
  }

  loading() {
    return this.isCurrent() && this.isLoading;
  }

  retrieveUsersByPhotoId() {
    this.getUsers.emit(this.image.photoId);
  }
}
